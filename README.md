# Another minimal awscli image

Another [Alpine](https://alpinelinux.org)-based minimal [aws-cli](https://github.com/aws/aws-cli) image.

## Usage

    docker run -e AWS_SECRET_KEY_ID=XYZ -e AWS_SECRET_ACCESS_KEY=ABC -e AWS_REGION=ap-southeast-2 -i --rm stanvit/awscli aws s3 ls
